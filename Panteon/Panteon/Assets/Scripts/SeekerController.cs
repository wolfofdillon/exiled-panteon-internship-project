﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CountPath))]
public class SeekerController : MonoBehaviour {

    CountPath counter;
    bool selected;
    public Sprite manSelectedImage;
    Sprite defaultSprite;
    Grid gridSystem;

    void Start () {
        counter = GetComponent<CountPath>();
        selected = false;
        defaultSprite = GetComponent<SpriteRenderer>().sprite;
        gridSystem = GameObject.FindGameObjectWithTag("PlayGround").GetComponent<Grid>();
    }


    public void OnMouseDown()
    {
        GameObject[] AllUnits = GameObject.FindGameObjectsWithTag("Man1");

        for (int i = 0; i < AllUnits.Length; i++) {
            AllUnits[i].GetComponent<SeekerController>().selectUnselect();
        }

        selected = true;
        this.gameObject.layer = 11;
        gridSystem.CreateGrid();

    }

    public void selectUnselect() {
        selected = false;
        this.gameObject.layer = 9;
    }

    void Update() {
        if (selected)
            this.GetComponent<SpriteRenderer>().sprite = manSelectedImage;
        else
            this.GetComponent<SpriteRenderer>().sprite = defaultSprite;

        if (Input.GetMouseButtonDown(1) && selected) {
            counter.FindPath(transform, GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()));
        }
        
    }


}