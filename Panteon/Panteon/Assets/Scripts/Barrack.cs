﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrack : MonoBehaviour {

    private void OnMouseDown()
    {
        GamePlayController.instance.BuildingSelected(gameObject.tag, GetComponent<SpriteRenderer>().sprite, gameObject.transform.position);

    }
}
