﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayController : MonoBehaviour
{

    public Text BuildingNameUI;
    public GameObject UnitButtonUI, UnitButtonUI2;
    public Image buildingImageUI;
    public Grid gridSystem;
    public Animator LeftPanelAnim, RightPanelAnim;
    Collider2D hitCollider;


    public GameObject Unit1, Unit2, camera;

    public static GamePlayController instance;
    int randomGridSpawnPoint;

    Vector2 buildingPos;

    private void Awake()
    {
        MakeSingleton();
        randomGridSpawnPoint = 0;
    }

    private void Update()
    {
        HandleCameraMovement();
    }

    private void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    // Sağdaki Info panelini düzenleyen fonksyon.
    public void BuildingSelected(string buildingName, Sprite buildingSprite, Vector2 buildingPos)
    {
        BuildingNameUI.text = buildingName;
        buildingImageUI.sprite = buildingSprite;
        this.buildingPos = buildingPos;
        if (buildingName.StartsWith("Bar"))
        {
            UnitButtonUI.SetActive(true);
            UnitButtonUI2.SetActive(true);
        }
        else if (buildingName.StartsWith("PP"))
        {
            UnitButtonUI.SetActive(false);
            UnitButtonUI2.SetActive(false);
        }

        RightPanelAnim.SetBool("buildingClicked", true);
    }

    public void UnitBtn(string unitName)
    {

        // Etrafındaki diğer binalara ve unitlere layer ile erişip, boş bulunan bir gride uniti çıkarmak için kullandım.
        int layerMask = (111111110);
        Vector2 unitPos = buildingPos;

        hitCollider = Physics2D.OverlapBox(unitPos + Vector2.right * 3, new Vector2(0.5f, 0.5f), 0f); // Başlangıç olarak binanın sağ kısmını aldım.

        // O grid boş olana kadar 4 yönde de boşluk bulmaya çalıştım. 
        tryAgain:
        if (hitCollider != null)
        {
            randomGridSpawnPoint = UnityEngine.Random.Range(0, 4);
            if (randomGridSpawnPoint == 0)
                unitPos += Vector2.up;
            else if (randomGridSpawnPoint == 1)
                unitPos += Vector2.right;
            else if (randomGridSpawnPoint == 2)
                unitPos += Vector2.down;
            else if (randomGridSpawnPoint == 3)
                unitPos += Vector2.left;

            if (unitPos.x >= 12.5f || unitPos.x <= -12.5f || unitPos.y <= -12.5f || unitPos.y >= 12.5f)
            {
                unitPos = buildingPos;
                goto tryAgain;
            }

            hitCollider = Physics2D.OverlapBox(unitPos, new Vector2(0.5f, 0.5f), 0f, layerMask);

            Debug.Log(unitPos);

            goto tryAgain;

        }

        // Grid boş olduğuna göre seçili unit o gridde spawn olabilir.

        if (unitName == "SpearMan")
            Instantiate(Unit1, unitPos, Quaternion.identity);
        else if (unitName == "ArcherMan")
            Instantiate(Unit2, unitPos, Quaternion.identity);

        // Yapılan değişikliğe göre grid sistemini tekrar oluşturuyorum.
        gridSystem.CreateGrid();
    }

    // Kamerayı yönetmek için kullanılan fonksyon.
    private void HandleCameraMovement()
    {
        if (Input.GetAxis("Mouse ScrollWheel") < 0) // forward
        {
            Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize + 0.2f, 5);
        }

        else if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize - 0.2f, 3);
        }

        if (Input.GetKey(KeyCode.S))
        {
            Vector3 temp = new Vector3(camera.transform.position.x, Mathf.Max(camera.transform.position.y - 0.1f, -7.5f), -10);
            camera.transform.position = temp;

            gridSystem.CreateGrid();
        }

        if (Input.GetKey(KeyCode.W))
        {
            Vector3 temp = new Vector3(camera.transform.position.x, Mathf.Min(camera.transform.position.y + 0.1f, 7.5f), -10);
            camera.transform.position = temp;

            gridSystem.CreateGrid();
        }

        if (Input.GetKey(KeyCode.D))
        {
            Vector3 temp = new Vector3(Mathf.Min(camera.transform.position.x + 0.1f, 3.6f), camera.transform.position.y, -10);
            camera.transform.position = temp;

            gridSystem.CreateGrid();
        }

        if (Input.GetKey(KeyCode.A))
        {
            Vector3 temp = new Vector3(Mathf.Max(camera.transform.position.x - 0.1f, -3.6f), camera.transform.position.y, -10);
            camera.transform.position = temp;

            gridSystem.CreateGrid();

        }
    }

    public void BuildingUIButtonClicked()
    {
        LeftPanelAnim.SetBool("buildingButtonClicked", !LeftPanelAnim.GetBool("buildingButtonClicked"));
    }

    // Verilen worldpos u alıp yuvarlayarak en yakın grid koordinat değerini veren fonksyon.
    public Vector2 SnapToGrid(Vector2 rawWorldPos)
    {
        float newX = Mathf.RoundToInt(rawWorldPos.x);
        float newY = Mathf.RoundToInt(rawWorldPos.y);
        return new Vector2(newX, newY);
    }

    // Mouse un o anki koordinatlarının worldpoint değerini bulan fonksyon.
    public Vector2 CalculateWorldPointOfMouseClick()
    {
        float mouseX = Input.mousePosition.x;
        float mouseY = Input.mousePosition.y;
        float distanceFromCamera = 10f;

        Vector3 wtf = new Vector3(mouseX, mouseY, distanceFromCamera);
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(wtf);

        return worldPos;
    }

}
