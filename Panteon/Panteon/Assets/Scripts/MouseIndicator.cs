﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseIndicator : MonoBehaviour {


    public void setItUp(Sprite buildingSprite) {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = buildingSprite;
        this.tag = buildingSprite.name.ToString();
    }
}
