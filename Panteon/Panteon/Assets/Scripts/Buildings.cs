﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buildings : MonoBehaviour {

    private void OnMouseDown()
    {
        Debug.Log(gameObject.transform.localPosition);
        GamePlayController.instance.BuildingSelected(gameObject.tag, GetComponent<SpriteRenderer>().sprite, gameObject.transform.localPosition);
    }
}
