﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingSpawner : MonoBehaviour
{

    public Camera myCamera;

    public GameObject mouseIndicator;

    public GameObject[] barracks;
    public GameObject[] PowerPlants;
    public MouseIndicator mouseIndicatorScript;

    public Grid grid;

    public bool buildingSelected;

    bool mouseIsInTheCanvas;

    private void Awake()
    {
        mouseIsInTheCanvas = false;
        buildingSelected = false;
    }

    private void Update()
    {
        ColorOfBuildingIndicator();
        CheckCancelCommand();
    }

    private void OnMouseExit()
    {
        mouseIsInTheCanvas = false;
        mouseIndicator.GetComponent<SpriteRenderer>().color = new Color(0,0,0,0);
    }

    private void OnMouseEnter()
    {
        mouseIsInTheCanvas = true;
    }

    // Verilen pozisyonda bir engel olup olmadığını döndüren fonksyon.
    bool CheckIfThereIsSmt(Vector2 desiredPosition) {
        int layerMask = 11 << 8;
        Vector2 buildingSize = new Vector2(2.5f, 2.5f);

        if (mouseIndicator.tag.StartsWith("PP"))
        {
            desiredPosition += Vector2.down /2;
            buildingSize = new Vector2(2.5f, 1.5f);
        }

        Collider2D hitCollider = Physics2D.OverlapBox(desiredPosition, buildingSize, 0f, layerMask);
        if (hitCollider != null)
        {
            mouseIndicator.GetComponent<SpriteRenderer>().color = Color.red;
            return true;
        }
        else
        {
            mouseIndicator.GetComponent<SpriteRenderer>().color = Color.white;
            return false;
        }


    }

    public void BuildingButtonClick(Sprite buildingSprite) {
        mouseIndicatorScript.setItUp(buildingSprite);
        buildingSelected = true;
    } 

    // O pozisyonda bir engel olup olmadığına göre göstergenin rengini değiştiren fonksyon.
    private void ColorOfBuildingIndicator() {
        if (mouseIsInTheCanvas && buildingSelected)
        {
            mouseIndicator.transform.position = GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick());
            if (CheckIfThereIsSmt(GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick())))
                mouseIndicator.GetComponent<SpriteRenderer>().color = Color.red;
            else mouseIndicator.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    // Oyun esnasında o anki yapılan işlemi durdurmak ve panelleri kapatmak için kullandığım fonksyon.
    private void CheckCancelCommand()
    {
        if (buildingSelected && Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.Escape))
        {
            mouseIndicator.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            GamePlayController.instance.RightPanelAnim.SetBool("buildingClicked", false);
            GamePlayController.instance.LeftPanelAnim.SetBool("buildingButtonClicked", false);
            buildingSelected = false;
        }
    }

    private void OnMouseDown()
    {
        if (buildingSelected)
        {
            if (!CheckIfThereIsSmt(GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick())))
            {
                if (mouseIndicator.tag == "Barrack0")
                    Instantiate(barracks[0], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);
                else if (mouseIndicator.tag == "Barrack1")
                    Instantiate(barracks[1], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);
                else if (mouseIndicator.tag == "Barrack2")
                    Instantiate(barracks[2], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);
                else if (mouseIndicator.tag == "Barrack3")
                    Instantiate(barracks[3], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);

                
                buildingSelected = false;
                mouseIndicator.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            }

            if (!CheckIfThereIsSmt(GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick())))
            {
                if (mouseIndicator.tag == "PP0")
                    Instantiate(PowerPlants[0], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);
                else if (mouseIndicator.tag == "PP1")
                    Instantiate(PowerPlants[1], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);
                else if (mouseIndicator.tag == "PP2")
                    Instantiate(PowerPlants[2], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);
                else if (mouseIndicator.tag == "PP3")
                    Instantiate(PowerPlants[3], GamePlayController.instance.SnapToGrid(GamePlayController.instance.CalculateWorldPointOfMouseClick()), Quaternion.identity);


                buildingSelected = false;
                mouseIndicator.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            }
           

        }

        grid.CreateGrid();
    }

}
